+++
categories = []
date = ""
title = "Formulario de Contacto"

+++
Si quieres ponerte en contacto con nosotros hay muchas formas. Puedes hacerlo a través de las redes sociales o escribiendo un correo electrónico o completando tus datos en el siguiente formulario:

<form name="contact" method="POST" data-netlify-recaptcha="true" data-netlify="true">
  <p>
    <label>Nombre: <input type="text" name="name" /></label>   
  </p>
  <p>
    <label>Email: <input type="email" name="email" /></label>
  </p>
  <p>
    <label>Teléfono: <input type="phone" name="phone" /></label>
  </p>
  <p>
    <label>Mensaje: <textarea name="message"></textarea></label>
  </p>
  <div data-netlify-recaptcha="true"></div>
  <p>
    <button type="submit">Enviar</button>
  </p>
</form>

Nos pondremos en contacto contigo a la dirección de correo electrónico indicada a la mayor brevedad porsible.