+++
categories = ["social"]
date = "2019-08-31T12:20:00+00:00"
title = "MG F-TF Drivers"

+++
Hace unos años atrás decidimos organizarnos alrededor del **MG F-TF Drivers club Catalunya,** llegando a tener más de 20 socios registrados.

El propósito era fomentar la afición por estos coches tan especiales y favorecer el disfrute entre amigos y **MG F-TF** con salidas combinando cultura, gastronomía y kilómetros de curvas donde los **MG F** i **MG TF** son los protagonistas.

Empezamos con la idea unos cuantos propietarios que nos encontrábamos en Montjuïc en una quedada junto a otros tantos propietarios de distintas marcas y que viven la misma pasión por los coches.

Por distintas razones el club se encuentra en un proceso de reestructuración pero los miembros queremos seguir disfrutando y conectándonos con todos los aficionados a los **MG F** y **MG TF**.

Éste es el único objetivo de esta web.

Nos gustaría y esperamos recibir un mensaje si has llegado hasta aquí y por supuesto, encontrarnos en las rutas.

Hasta pronto!!