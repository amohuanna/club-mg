+++
categories = ["social"]
date = "2019-10-02T22:00:00+00:00"
title = "XII Jornada AECC"

+++
La **AECC-Catalunya contra el Cáncer de Barcelona**, con la colaboración del Circuito de Barcelona-Catalunya, organizan el **sábado 5 de octubre** la **XII Jornada AECC & Circuito de Barcelona - Catalunya**.

Allí estaremos!!

![](/uploads/Screenshot 2019-10-05 at 00.06.35.png)