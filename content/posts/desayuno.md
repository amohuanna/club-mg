---
title: Desayuno en Teià 15/9/19
date: 2019-08-26T09:24:25+00:00
categories:
- quedada

---
Para aquel que quiera juntarse a pasar un rato quedamos el domingo 15 de septiembre en el Club Tennis Barcelona-Teià a las 9:30-10:00 para tomar el desayuno, conversar y planear la próxima quedada.

Todos son bienvenidos,

¿Te apuntas?

![](https://cdn.restaurantes.com/static/img/restaurants/841/84146/84146_6403.gl.jpg)

http://www.ctbteia.com/