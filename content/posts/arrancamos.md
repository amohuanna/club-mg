---
title: Arrancamos...
date: 2019-08-25T13:11:08.000+00:00
categories: ["social"]
---
Después de una larga inactividad y para no perder el espíritu que caracteriza a todos aquellos afortunados propietarios 
de un MG-F o MG-TF, les invitamos a retomar el contacto y sumarse a este espacio para poder organizar salidas o
simplemente quedadas para conversar y pasar un buen momento.
