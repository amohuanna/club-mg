+++
categories = ["archivo"]
date = "2019-08-28T21:00:00+00:00"
title = "Ruta Alt Penedès 03/18"

+++
Ruta por el Alt Penedès

Punt de trobada: 10:30 Ace Cafe Barcelona  
Carrer de les Ciències, 105, 08908 Hospitalet de Llobregat, Barcelona  
Primer tram: 11:00 a 12:00  
Sortida de Ace Cafe Barcelona camí de les Caves Joan Sardà.

La sortida serà puntual a les 11h per tal de poder arribar a la visita de les Caves Joan Sardà que esta reservada a les 12h.

Visita a les Caves Joan Sardà:12:00 a 13:00  
Es visitaran les vinyes, situades al voltant del celler que gaudeixen dels privilegis naturals de la zona del Penedès, coneixent cada varietat de raïm.  
La planta de producció on es combina la tradició i l’automatització per a aconseguir un producte final de qualitat.  
La sala de criança on el temps atorga al vi la maduresa adient gràcies a una temperatura estable i una humitat controlada, condició imprescindible per a un bon envelliment tant a la bóta de roure com a l’ampolla.  
La zona d’expedició on s’hi emmagatzema el producte final que serà enviat a diferents mercats tant nacionals com internacionals  
Segon tram:13:00 a 13:30  
Sortida de Caves Joan Sardà fins al restaurant Celler d’Aiguaviva

Dinar:13:30 - 14:00 aprox.  
A les 14 es dinarà en el Celler d’Aiguaviva

Despeses:  
Visita a els Caves Joan Sardà i dinar al restaurant El Celler d’Aiguaviva 35€ per persona

![](/uploads/Screenshot 2019-08-28 at 23.25.33.png)